package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState initialState;

    private CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        
        grid = new CellState[rows][columns];
        for (int row = 0; row<rows; row++){
            for (int col= 0; col<columns; col++){
                grid[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return grid.length;
    }

    @Override
    public int numColumns() {
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {

        if(row<0 || row >= rows)
			throw new IndexOutOfBoundsException("Illegal row value");
		if(column<0 || column >= columns)
			throw new IndexOutOfBoundsException("Illegal column value");
        
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if(row<0 || row >= rows)
			throw new IndexOutOfBoundsException("Illegal row value");
		if(column<0 || column >= columns)
			throw new IndexOutOfBoundsException("Illegal column value");

        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newCellGrid = new CellGrid(numRows(), numColumns(), initialState);

        for (int row = 0; row<rows; row++){
            for (int col= 0; col<columns; col++){
                CellState newRowAndCol = get(row, col);
                newCellGrid.set(row, col, newRowAndCol);
            }
        }
        return newCellGrid;
    }
    
}
