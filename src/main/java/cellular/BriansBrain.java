package cellular;

import datastructure.IGrid;
import datastructure.CellGrid;
import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid brain;

    public BriansBrain(int rows, int columns) {
		brain = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        return brain.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < brain.numRows(); row++) {
			for (int col = 0; col < brain.numColumns(); col++) {
				if (random.nextBoolean()) {
					brain.set(row, col, CellState.ALIVE);
				} else {
					brain.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        IGrid newBrain = brain.copy();
		for (int r = 0; r< numberOfRows(); r++){
            for (int c= 0; c< numberOfColumns(); c++){
                CellState nextStep = getNextCell(r,c);
				newBrain.set(r, c, nextStep);
            }
        }
        brain = newBrain;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = getCellState(row,col);
        if (state == CellState.ALIVE){
            state = CellState.DYING;
        }
        else if (state == CellState.DYING){
            state = CellState.DEAD;
        }
        else{
            if (countNeighbors(row, col, CellState.ALIVE) == 2){
                state = CellState.ALIVE;
            }
            else{
                state = CellState.DEAD;
            }
            
        }
        return state;
    }

    private int countNeighbors(int row, int col, CellState state) {
		int countState = 0;
		for (int r = row-1; r<= row+1; r++){
            if(r < 0 || r >= numberOfRows()) continue;
            for (int c=col-1; c<= col+1; c++){
                if(c < 0 || c >= numberOfColumns()) continue;
				CellState neighborState = getCellState(r,c);
                if (neighborState == CellState.ALIVE){
					if (r == row && c == col){
						continue;
					}
					countState++;
				}
            }
        }
		return countState;
	}

    @Override
    public int numberOfRows() {
        return brain.numRows();
    }

    @Override
    public int numberOfColumns() {
        return brain.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return brain;
    }
    
}
